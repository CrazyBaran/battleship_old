﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Entities
{
    public interface IFireable
    {
        bool IsFireable { get; }
        bool Fire(); 
    }
}
