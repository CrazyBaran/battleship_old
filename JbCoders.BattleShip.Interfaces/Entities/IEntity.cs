﻿using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Entities
{
    public interface IEntity : IDrawable, ISetable, IFireable
    {
    }
}
