﻿using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Games
{
    public interface ICommand
    {
        string X { get; }
        int? Y { get; }
        bool Execute(IPanel panel);
    }
}
