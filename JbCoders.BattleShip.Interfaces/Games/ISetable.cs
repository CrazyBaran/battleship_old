﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Games
{
    public interface ISetable
    {
        bool IsSetable { get; }
        void MakeUnsetable();
    }
}
