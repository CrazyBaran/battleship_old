﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Games
{
    public interface IStepService
    {
        IGameStep NextStep();
        bool FireTime { get; }
    }
}
