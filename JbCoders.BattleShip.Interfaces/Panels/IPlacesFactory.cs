﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Panels
{
    public interface IPlacesFactory
    {
        List<IPlace> Init(int i);
    }
}
