﻿using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Panels
{
    public interface IPanel : IDrawable
    {
        bool Fire(string x, int y);

        bool SetShip(string x, int y, int size, bool direction);

        IReadOnlyList<IPlace> Places { get; }
    }
}
