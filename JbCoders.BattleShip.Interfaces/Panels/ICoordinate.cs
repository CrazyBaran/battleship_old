﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Panels
{
    public interface ICoordinate
    {
        string X { get; }
        int Y { get; }
    }
}
