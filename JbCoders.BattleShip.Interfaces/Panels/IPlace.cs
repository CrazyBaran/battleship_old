﻿using JbCoders.BattleShip.Interfaces.Entities;
using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Interfaces.Panels
{
    public interface IPlace : IDrawable, ISetable, IFireable, ICoordinate
    {
        bool SetEntity(IEntity entity);
    }
}
