﻿using JbCoders.BattleShip.Engine.Steps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class FireStepTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void WriteStepTest()
        {
            var expected = "Give coordinates to shot: eg. A5";
            var fireStep = new FireStep();

            Assert.AreEqual(expected, fireStep.WriteStep());
        }

        [TestCase("A5", "A", 5)]
        [TestCase("A 5", "A", 5)]
        [TestCase("A05", "A", 5)]
        [TestCase("A 05", "A", 5)]
        public void ReadStepTest(string input, string expectedX, int expectedY)
        {
            var fireStep = new FireStep();
            var actual = fireStep.ReadStep(input);
            Assert.AreEqual(expectedX, actual.X);
            Assert.AreEqual(expectedY, actual.Y);
        }
    }
}
