﻿using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Engine.Panels;
using JbCoders.BattleShip.Interfaces.Panels;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class PanelTests
    {
        [Test]
        public void InitializationTest()
        {
            var panel = new Panel(new PlacesFactory(), 10, false);
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);

                panel.Draw();

                var actual = sw.ToString();
                var expected = "    A B C D E F G H I J \r\n 01 * * * * * * * * * * \r\n 02 * * * * * * * * * * \r\n 03 * * * * * * * * * * \r\n 04 * * * * * * * * * * \r\n 05 * * * * * * * * * * \r\n 06 * * * * * * * * * * \r\n 07 * * * * * * * * * * \r\n 08 * * * * * * * * * * \r\n 09 * * * * * * * * * * \r\n 10 * * * * * * * * * * \r\n            \r\n";
                Assert.AreEqual(expected, actual);
            }
        }

        [Test]
        public void FillWaterTest()
        {
            var panel = new Panel(new PlacesFactory(), 10, false);
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);

                panel.FillWater();
                panel.Draw();

                var actual = sw.ToString();
                var expected = "    A B C D E F G H I J \r\n 01 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 02 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 03 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 04 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 05 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 06 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 07 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 08 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 09 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n 10 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \r\n            \r\n";
                Assert.AreEqual(expected, actual);
            }
        }

        [Test]
        public void OverPanelFireTest()
        {
            var panel = new Panel(new PlacesFactory(), 1, false);
            var actual = panel.Fire("C", 3);

            Assert.AreEqual(false, actual);
        }

        [Test]
        public void FireTest()
        {
            var mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.Fire()).Returns(true);
            mockPlace.Setup(foo => foo.X).Returns("A");
            mockPlace.Setup(foo => foo.Y).Returns(1);
            mockPlace.Setup(foo => foo.IsFireable).Returns(true);
            var mockPlacesFactory = new Mock<IPlacesFactory>();
            mockPlacesFactory.Setup(foo => foo.Init(It.IsAny<int>()))
                .Returns((int x) => new List<IPlace>() { mockPlace.Object });

            var panel = new Panel(mockPlacesFactory.Object, It.IsAny<int>(), false);
            panel.Fire("A", 1);

            mockPlace.Verify(foo => foo.Fire());
        }

        [Test]
        public void SymbolFireTest()
        {
            var panel = new Panel(new PlacesFactory(), 10);
            var actual = panel.Fire("@", 0);

            Assert.AreEqual(false, actual);
        }

        [Test]
        public void WaterFireTest()
        {
            var panel = new Panel(new PlacesFactory(), 10);
            panel.FillWater();
            var actual = panel.Fire("A", 1);

            Assert.AreEqual(true, actual);
        }

        [Test]
        public void DoubleWaterFireTest()
        {
            var panel = new Panel(new PlacesFactory(), 10);
            panel.FillWater();
            panel.Fire("A", 1);
            var actual = panel.Fire("A", 1);

            Assert.AreEqual(false, actual);
        }


    }
}

