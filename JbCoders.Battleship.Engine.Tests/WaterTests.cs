﻿using JbCoders.BattleShip.Engine.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class WaterTests
    {
        [Test]
        public void WaterFire()
        {
            var water = new Water();

            water.Fire();

            Assert.AreEqual(false, water.IsFireable);
        }

        [Test]
        public void DoubleWaterFire()
        {
            var water = new Water();

            water.Fire();
            var actual = water.Fire();

            Assert.AreEqual(false, actual);
        }

        [TestCase("~", false)]
        [TestCase("@", true)]
        public void WaterDisplayTest(string expected, bool fire)
        {
            var water = new Water();

            if (fire) water.Fire();

            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);

                water.Draw();

                var actual = sw.ToString();
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
