﻿using JbCoders.BattleShip.Engine.Players;
using JbCoders.BattleShip.Interfaces.Games;
using JbCoders.BattleShip.Interfaces.Panels;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class ComputerPlayerTests
    {
        private Mock<IPanel> _panel;
        private Mock<IStepService> _stepService;
        [SetUp]
        public void SetUp()
        {
            var place = new Mock<IPlace>();
            place.Setup(foo => foo.X).Returns("A");
            place.Setup(foo => foo.Y).Returns(1);
            place.Setup(foo => foo.IsSetable).Returns(true);
            place.Setup(foo => foo.IsFireable).Returns(true);

            _panel = new Mock<IPanel>();
            _panel.Setup(foo => foo.Places).Returns(new List<IPlace>() { place.Object });

            _stepService = new Mock<IStepService>();
        }

        [Test]
        public void FireStepTest()
        {
            _stepService.Setup(foo => foo.FireTime).Returns(true);
            var fireCommand = new Mock<ICommand>();
            fireCommand.Setup(foo => foo.Execute(It.IsAny<IPanel>())).Returns(true);
            var fireStep = new Mock<IGameStep>();
            fireStep.Setup(foo => foo.ReadStep(It.IsAny<string>()))
                .Returns(fireCommand.Object);
            _stepService.Setup(foo => foo.NextStep()).Returns(fireStep.Object);

            var computerPlayer = new ComputerPlayer(_panel.Object, 
                _panel.Object, _stepService.Object);

            computerPlayer.Turn();

            fireStep.Verify(foo => foo.ReadStep(It.IsAny<string>()));
        }

        [Test]
        public void SetStepTest()
        {
            _stepService.Setup(foo => foo.FireTime).Returns(false);
            var fireCommand = new Mock<ICommand>();
            fireCommand.Setup(foo => foo.Execute(It.IsAny<IPanel>())).Returns(true);
            var fireStep = new Mock<IGameStep>();
            fireStep.Setup(foo => foo.ReadStep(It.IsAny<string>()))
                .Returns(fireCommand.Object);
            _stepService.Setup(foo => foo.NextStep()).Returns(fireStep.Object);

            var computerPlayer = new ComputerPlayer(_panel.Object,
                _panel.Object, _stepService.Object);

            computerPlayer.Turn();

            fireStep.Verify(foo => foo.ReadStep(It.IsAny<string>()));
        }
    }
}
