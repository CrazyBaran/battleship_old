﻿using JbCoders.BattleShip.Engine.Steps;
using JbCoders.BattleShip.Interfaces.Panels;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class FireCommandTests
    {
        [Test]
        public void ExecuteTest()
        {
            var fireCommand = new FireCommand("A", 5);

            var mockPanel = new Mock<IPanel>();
            mockPanel.Setup(foo => foo.Fire(It.Is<string>(i => i == "A"), It.Is<int>(i => i == 5)));

            fireCommand.Execute(mockPanel.Object);

            mockPanel.VerifyAll();
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ReturnExecuteTest(bool expected)
        {
            var fireCommand = new FireCommand("A", 5);

            var mockPanel = new Mock<IPanel>();
            mockPanel.Setup(foo => foo.Fire(It.IsAny<string>(), It.IsAny<int>()))
                .Returns((string x, int y) => expected);

            var actual = fireCommand.Execute(mockPanel.Object);
            Assert.AreEqual(expected, actual);
        }
    }
}
