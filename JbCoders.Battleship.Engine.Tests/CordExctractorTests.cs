﻿using JbCoders.BattleShip.Engine.Steps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class CordExctractorTests
    {
        [TestCase("A1V", "A")]
        public void ExtractXTest(string input, string expected)
        {
            var actual = CordExctractor.ExtractX(input);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("A1V", 1)]
        [TestCase("A10V", 10)]
        public void ExtractYTest(string input, int expected)
        {
            var actual = CordExctractor.ExtractY(input);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("A1V", false)]
        [TestCase("A10V", false)]
        [TestCase("A1H", true)]
        [TestCase("A10H", true)]
        public void ExtractDirectionTest(string input, bool expected)
        {
            var actual = CordExctractor.ExtractDirection(input);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("A1V", true)]
        [TestCase("A10V", true)]
        [TestCase("A1H", true)]
        [TestCase("A10H", true)]
        [TestCase("A100H", false)]
        [TestCase("A1VAaaa", false)]
        [TestCase("10AV", false)]
        [TestCase("A1", false)]
        [TestCase("A", false)]
        public void IsValidSetShip(string input, bool expected)
        {
            var actual = CordExctractor.IsValidSetShip(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
