﻿using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Engine.Panels;
using JbCoders.BattleShip.Interfaces.Panels;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class PanelWithManyPlacesTests
    {
        Mock<IPlacesFactory> mockPlacesFactory = null;
        List<Mock<IPlace>> listMockPlaces = null;

        [SetUp]
        public void SetUp()
        {
            listMockPlaces = new List<Mock<IPlace>>();
            var mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("A");
            mockPlace.Setup(foo => foo.Y).Returns(1);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("B");
            mockPlace.Setup(foo => foo.Y).Returns(1);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("C");
            mockPlace.Setup(foo => foo.Y).Returns(1);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("D");
            mockPlace.Setup(foo => foo.Y).Returns(1);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("A");
            mockPlace.Setup(foo => foo.Y).Returns(2);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("B");
            mockPlace.Setup(foo => foo.Y).Returns(2);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("C");
            mockPlace.Setup(foo => foo.Y).Returns(2);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("D");
            mockPlace.Setup(foo => foo.Y).Returns(2);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("A");
            mockPlace.Setup(foo => foo.Y).Returns(3);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("B");
            mockPlace.Setup(foo => foo.Y).Returns(3);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("C");
            mockPlace.Setup(foo => foo.Y).Returns(3);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("D");
            mockPlace.Setup(foo => foo.Y).Returns(3);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("A");
            mockPlace.Setup(foo => foo.Y).Returns(4);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("B");
            mockPlace.Setup(foo => foo.Y).Returns(4);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("C");
            mockPlace.Setup(foo => foo.Y).Returns(4);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlace = new Mock<IPlace>();
            mockPlace.Setup(foo => foo.X).Returns("D");
            mockPlace.Setup(foo => foo.Y).Returns(4);
            mockPlace.Setup(foo => foo.IsSetable).Returns(true);
            mockPlace.Setup(foo => foo.MakeUnsetable());
            listMockPlaces.Add(mockPlace);
            mockPlacesFactory = new Mock<IPlacesFactory>();
            mockPlacesFactory.Setup(foo => foo.Init(It.IsAny<int>()))
                .Returns((int x) => listMockPlaces.Select(y => y.Object).ToList());

        }

        [TearDown]
        public void TearDown()
        {
            mockPlacesFactory = null;
            listMockPlaces = null;
        }

        [Test]
        public void SetShipTest()
        {
            var panel = new Panel(mockPlacesFactory.Object, It.IsAny<int>());

            panel.SetShip("B", 2, 1, true);

            listMockPlaces.First(x=>x.Object.X == "B" && x.Object.Y == 2)
                .Verify(foo => foo.SetEntity(It.IsAny<Ship>()));
        }

        [Test]
        public void NearIsSetableTest()
        {
            var panel = new Panel(mockPlacesFactory.Object, It.IsAny<int>());

            panel.SetShip("B", 2, 1, true);

            listMockPlaces.First(x => x.Object.X == "A" && x.Object.Y == 1).Verify(x => x.MakeUnsetable());
            listMockPlaces.First(x => x.Object.X == "B" && x.Object.Y == 1).Verify(x => x.MakeUnsetable());
            listMockPlaces.First(x => x.Object.X == "C" && x.Object.Y == 1).Verify(x => x.MakeUnsetable());
            listMockPlaces.First(x => x.Object.X == "A" && x.Object.Y == 2).Verify(x => x.MakeUnsetable());
            listMockPlaces.First(x => x.Object.X == "C" && x.Object.Y == 2).Verify(x => x.MakeUnsetable());
            listMockPlaces.First(x => x.Object.X == "A" && x.Object.Y == 3).Verify(x => x.MakeUnsetable());
            listMockPlaces.First(x => x.Object.X == "B" && x.Object.Y == 3).Verify(x => x.MakeUnsetable());
            listMockPlaces.First(x => x.Object.X == "C" && x.Object.Y == 3).Verify(x => x.MakeUnsetable());
        }

        [Test]
        public void SetShipVertical()
        {
            var panel = new Panel(mockPlacesFactory.Object, It.IsAny<int>());

            panel.SetShip("B", 2, 2, false);

            listMockPlaces.First(x => x.Object.X == "B" && x.Object.Y == 2)
                .Verify(foo => foo.SetEntity(It.IsAny<Ship>()));
            listMockPlaces.First(x => x.Object.X == "B" && x.Object.Y == 3)
                .Verify(foo => foo.SetEntity(It.IsAny<Ship>()));
        }

        [Test]
        public void SetShipHorizontal()
        {
            var panel = new Panel(mockPlacesFactory.Object, It.IsAny<int>());

            panel.SetShip("B", 2, 2, true);

            listMockPlaces.First(x => x.Object.X == "B" && x.Object.Y == 2)
                .Verify(foo => foo.SetEntity(It.IsAny<Ship>()));
            listMockPlaces.First(x => x.Object.X == "C" && x.Object.Y == 2)
                .Verify(foo => foo.SetEntity(It.IsAny<Ship>()));
        }

        [Test]
        public void SetNearOtherShip()
        {
            var panel = new Panel(new PlacesFactory(), 3);

            panel.FillWater();
            panel.SetShip("B", 2, 1, true);
            var actual = panel.SetShip("B", 3, 1, true);

            Assert.AreEqual(false, actual);
        }
    }
}
