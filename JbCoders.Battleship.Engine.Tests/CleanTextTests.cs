﻿using JbCoders.BattleShip.Engine.Steps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class CleanTextTests
    {
        [TestCase(",", "")]
        [TestCase(", ", "")]
        [TestCase(" ", "")]
        [TestCase(":", "")]
        [TestCase(";", "")]
        [TestCase("A b C", "ABC")]
        [TestCase("A1 b3", "A1B3")]
        public void CleanTest(string input, string expected)
        {
            var actual = CleanText.Clean(input);
            Assert.AreEqual(expected, actual);
        }
    }
}
