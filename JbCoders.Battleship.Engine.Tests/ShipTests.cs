﻿using JbCoders.BattleShip.Engine.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class ShipTests
    {
        [TestCase("#", false, false)]
        [TestCase("~", false, true)]
        [TestCase("X", true, false)]
        [TestCase("X", true, true)]
        public void ShipDisplayTest(string expected, bool fire, bool cover)
        {
            var ship = new Ship(cover);

            if (fire) ship.Fire();

            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);

                ship.Draw();

                var actual = sw.ToString();
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
