﻿using JbCoders.BattleShip.Engine.Steps;
using JbCoders.BattleShip.Interfaces.Panels;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.Battleship.Engine.Tests
{
    public class SetShipStepTests
    {
        [Test]
        public void WriteStepTest()
        {
            var expected = "Give coordinates for ship and direction eg. A5 V / B2 H";
            var setShipStep = new SetShipStep(1);

            Assert.AreEqual(expected, setShipStep.WriteStep());
        }

        [TestCase("A5 V", "A", 5, false)]
        [TestCase("A 5V", "A", 5, false)]
        [TestCase("A05H", "A", 5, true)]
        [TestCase("A 05 H", "A", 5, true)]
        public void ReadStepTest(string input, string expectedX, int expectedY, bool expectedDir)
        {
            var mockPanel = new Mock<IPanel>();
            mockPanel.Setup(foo => foo.SetShip(It.Is<string>(x => x == expectedX),
                                                It.Is<int>(x => x == expectedY),
                                                It.Is<int>(x => x == 1),
                                                It.Is<bool>(x => x == expectedDir))).Returns(true);
            var setShipStep = new SetShipStep(1);
            var actual = setShipStep.ReadStep(input);

            actual.Execute(mockPanel.Object);

            mockPanel.VerifyAll();
        }
    }
}
