﻿using JbCoders.BattleShip.Engine.Steps;
using JbCoders.BattleShip.Interfaces.Games;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Players
{
    public abstract class Player : IPlayer
    {
        protected readonly IPanel _myPanel;
        protected readonly IPanel _enemyPanel;
        protected readonly IStepService _stepService;
        public Player(IPanel panel, IPanel enemyPanel, IStepService stepService)
        {
            _myPanel = panel ?? throw new ArgumentNullException(nameof(panel));
            _enemyPanel = enemyPanel ?? throw new ArgumentNullException(nameof(panel));
            _stepService = stepService ?? throw new ArgumentNullException(nameof(stepService));
        }

        public void Draw()
        {
            _myPanel.Draw();
        }

        protected abstract string Input();
        protected abstract void Output(string text);
        public bool Turn()
        {

            var step = _stepService.NextStep();
            var panel = _stepService.FireTime ? _enemyPanel : _myPanel;
            ICommand command = null;
            do
            {
                Output(step.WriteStep());
                command = step.ReadStep(Input());
                if (command is ExitCommand) return false;
            } while (!(command != null && command.Execute(panel)));
            return true;
        }
    }
}
