﻿using JbCoders.BattleShip.Engine.Steps;
using JbCoders.BattleShip.Interfaces.Games;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Players
{
    public class HumanPlayer : Player
    {
        public HumanPlayer(IPanel panel, IPanel enemyPanel, IStepService stepService) 
            : base(panel, enemyPanel, stepService)
        {
        }

        protected override string Input()
        {
            return Console.ReadLine();
        }

        protected override void Output(string text)
        {
            Console.WriteLine(text);
        }
    }
}
