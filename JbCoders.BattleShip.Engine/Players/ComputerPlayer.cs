﻿using JbCoders.BattleShip.Interfaces.Games;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.BattleShip.Engine.Players
{
    public class ComputerPlayer : Player
    {
        private readonly Random _random = new Random();
        public ComputerPlayer(IPanel panel, IPanel enemyPanel, IStepService stepService)
            : base(panel, enemyPanel, stepService)
        {
        }

        private string RandomSetShip()
        {
            var setable = _enemyPanel.Places.Where(x => x.IsSetable).ToList();
            var setTarget = setable[_random.Next() % setable.Count];
            var dir = _random.Next() % 2 == 0 ? "V" : "H";
            return $"{setTarget.X}{setTarget.Y}{dir}";
        }

        private string RandomFire()
        {
            var fireable = _enemyPanel.Places.Where(x => x.IsFireable).ToList();
            var fireTarget = fireable[_random.Next() % fireable.Count];
            return $"{fireTarget.X}{fireTarget.Y}";
        }

        protected override string Input()
        {
            return _stepService.FireTime ? RandomFire() : RandomSetShip();
        }

        protected override void Output(string text)
        {

        }
    }
}
