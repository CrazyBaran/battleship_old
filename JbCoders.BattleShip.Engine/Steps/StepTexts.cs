﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public static class StepTexts
    {
        public static string FireText = "Give coordinates to shot: eg. A5";

        public static string SetShip = "Give coordinates for ship and direction eg. A5 V / B2 H";
    }
}
