﻿using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public class FireStep : ExitStep, IGameStep
    {
        public ICommand ReadStep(string input)
        {
            input = CleanText.Clean(input);
            if(IsExit(input))
            {
                return Exit(input);
            }
            if (!CordExctractor.IsValidFire(input)) return null;
            var x = CordExctractor.ExtractX(input);
            var y = CordExctractor.ExtractY(input);

            return new FireCommand(x, y.Value);
        }

        public string WriteStep()
        {
            return StepTexts.FireText;
        }
    }
}
