﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public static class CleanText
    {
        public static string Clean(string input)
        {
            input = Trim(input);
            input = ToUpper(input);
            input = CleanEmpty(input);
            return input;
        }

        private static string ToUpper(string input)
        {
            return input.ToUpper();
        }

        private static string Trim(string input)
        {
            return input.Trim();
        }
        private static string CleanEmpty(string input)
        {
            return input.Replace(" ", "").Replace(",", "").Replace(";", "").Replace(":","");
        }
    }
}
