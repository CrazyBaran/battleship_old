﻿using JbCoders.BattleShip.Interfaces.Games;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public class FireCommand : ICommand
    {
        public FireCommand(string x, int y)
        {
            X = x;
            Y = y;
        }
        public string X { get; }

        public int? Y { get; }

        public bool Execute(IPanel panel)
        {
            return panel.Fire(X, Y.Value);
        }
    }
}
