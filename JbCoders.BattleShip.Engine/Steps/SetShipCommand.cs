﻿using JbCoders.BattleShip.Interfaces.Games;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public class SetShipCommand : ICommand
    {
        public SetShipCommand(string x, int y, bool direction, int size)
        {
            X = x;
            Y = y;
            _direction = direction;
            _size = size;
        }
        private int _size;
        private bool _direction;
        public string X { get; }

        public int? Y { get; }

        public bool Execute(IPanel panel)
        {
            return panel.SetShip(X, Y.Value, _size, _direction);
        }
    }
}
