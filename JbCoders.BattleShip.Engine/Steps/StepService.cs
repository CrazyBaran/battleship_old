﻿using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public class StepService : IStepService
    {
        private readonly List<int> _shipSize;
        private int stepNo = -1;

        public bool FireTime => !(stepNo < _shipSize.Count);

        public StepService()
        {
            _shipSize = new List<int>();
            _shipSize.Add(5);
            _shipSize.Add(4);
            _shipSize.Add(4);
        }

        private IGameStep ReturnShipStep(int size)
        {
            return new SetShipStep(size);
        }

        private IGameStep ReturnFireStep()
        {
            return new FireStep();
        }
        public IGameStep NextStep()
        {
            stepNo++;
            if(stepNo < _shipSize.Count)
            {
                return ReturnShipStep(_shipSize[stepNo]);
            }
            else
            {
                return ReturnFireStep();
            }
        }
    }
}
