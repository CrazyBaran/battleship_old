﻿using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public class SetShipStep : ExitStep, IGameStep
    {
        private int _size;
        public SetShipStep(int size)
        {
            _size = size;
        }
        public ICommand ReadStep(string input)
        {
            input = CleanText.Clean(input);
            if (IsExit(input))
            {
                return Exit(input);
            }
            if (!CordExctractor.IsValidSetShip(input)) return null;
            var x = CordExctractor.ExtractX(input);
            var y = CordExctractor.ExtractY(input);
            var dir = CordExctractor.ExtractDirection(input);

            return new SetShipCommand(x, y.Value, dir, _size);
        }

        public string WriteStep()
        {
            return StepTexts.SetShip;
        }
    }
}
