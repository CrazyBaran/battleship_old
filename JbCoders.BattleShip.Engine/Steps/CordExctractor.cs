﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace JbCoders.BattleShip.Engine.Steps
{
    public static class CordExctractor
    {
        public static bool IsValidFire(string input)
        {
            var regex = new Regex(@"^[a-zA-Z]{1}\d{1,2}$");
            return regex.IsMatch(input);
        }
        public static bool IsValidSetShip(string input)
        {
            var regex = new Regex(@"^[a-zA-Z]{1}\d{1,2}[vhVH]{1}\Z");
            return regex.IsMatch(input);
        }
        private static int FirstNumber(string input)
        {
            return input.IndexOf(input.First(x => Char.IsDigit(x)));
        }

        private static int LastNumber(string input)
        {
            return input.LastIndexOf(input.Last(x => Char.IsDigit(x)));
        }
        public static string ExtractX(string input)
        {
            var first = FirstNumber(input);
            var returnInput = input.Remove(first);
            return returnInput;
        }

        public static int? ExtractY(string input)
        {
            input = input.Remove(0, FirstNumber(input)).Remove(LastNumber(input), input.Count() - LastNumber(input) - 1);
            if (int.TryParse(input, out int i))
            {
                return i;
            }
            return null;
        }

        public static bool ExtractDirection(string input)
        {
            if(input.Last() == 'H')
            {
                return true;
            }
            return false;
        }
    }
}
