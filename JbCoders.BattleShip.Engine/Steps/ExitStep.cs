﻿using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Steps
{
    public class ExitStep 
    {
        protected bool IsExit(string input)
        {
            if (input == "X")
            {
                return true;
            }
            return false;
        }
        protected ICommand Exit(string input)
        {
            if(IsExit(input))
            {
                return new ExitCommand();
            }
            return null;
        }
    }
}
