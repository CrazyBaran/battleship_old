﻿using JbCoders.BattleShip.Interfaces.Entities;
using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public abstract class Entity : IDrawable, IEntity
    {
        public virtual bool IsSetable => throw new NotImplementedException();

        public virtual bool IsFireable => throw new NotImplementedException();

        public abstract void Draw();

        public abstract bool Fire();

        public abstract void MakeUnsetable();
    }
}
