﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class Water : FireEntity
    {
        private bool _isSetable = true;
        public override void Draw()
        {
            if (_fired)
            {
                Console.Write("@");
            }
            else
            {
                Console.Write("~");
            }
        }

        public override void MakeUnsetable()
        {
            _isSetable = false;
        }

        public override bool IsFireable => !_fired;
        public override bool IsSetable => _isSetable;
    }
}
