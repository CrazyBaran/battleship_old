﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class Symbol : Entity
    {
        private readonly string _text;
        public Symbol(string symbol)
        {
            _text = symbol;
        }
        public override void Draw()
        {
            Console.Write(_text);
        }

        public override bool Fire()
        {
            throw new InvalidOperationException("Cant fire symbol");
        }

        public override void MakeUnsetable()
        {
        }

        public override bool IsFireable => false;
        public override bool IsSetable => false;
    }
}
