﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public abstract class FireEntity : Entity
    {
        protected bool _fired;
        public override bool Fire()
        {
            if (IsFireable)
            {
                _fired = true;
                return true;
            }
            return false;
        }
    }
}
