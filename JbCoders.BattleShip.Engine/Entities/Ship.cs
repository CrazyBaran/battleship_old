﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class Ship : FireEntity
    {
        bool _cover;
        public Ship(bool cover)
        {
            _cover = cover;
        }
        public override void Draw()
        {
            if(_fired)
            {
                Console.Write("X");
            }
            else if(_cover)
            {
                Console.Write("~");
            }
            else
            {
                Console.Write("#");
            }
        }

        public override void MakeUnsetable()
        {
            
        }

        public override bool Fire()
        {
            if (!_fired) Console.Beep();
            return base.Fire();
        }

        public override bool IsSetable => false;

        public override bool IsFireable => !_fired;
    }
}
