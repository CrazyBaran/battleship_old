﻿using JbCoders.BattleShip.Engine.Panels;
using JbCoders.BattleShip.Engine.Players;
using JbCoders.BattleShip.Engine.Steps;
using JbCoders.BattleShip.Interfaces.Games;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine
{
    public class BattleShipGame : IGame
    {
        private readonly IPlayer _humanPlayer;
        private readonly IPlayer _computerPlayer;
        public BattleShipGame()
        {
            var humanPanel = new Panel(new PlacesFactory(), 10, false);
            humanPanel.FillWater();
            var computerPanel = new Panel(new PlacesFactory(), 10, true);
            computerPanel.FillWater();
            _humanPlayer = new HumanPlayer(humanPanel, computerPanel, new StepService());
            _computerPlayer = new ComputerPlayer(computerPanel, humanPanel, new StepService());
        }
        public void Start()
        {
            while (true)
            {
                Console.WriteLine("Computer:");
                _computerPlayer.Draw();
                Console.WriteLine("Player:");
                _humanPlayer.Draw();
                if (!_humanPlayer.Turn()) break;
                if (!_computerPlayer.Turn()) break;
            }
            Console.ReadKey();
        }
    }
}
