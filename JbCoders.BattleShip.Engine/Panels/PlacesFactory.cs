﻿using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Panels
{
    public class PlacesFactory : IPlacesFactory
    {
        public List<IPlace> Init(int size)
        {
            var _places = new List<IPlace>();
            _places.AddRange(InitUpBar(size));
            for (int i = 1; i <= size; i++)
            {
                _places.AddRange(InitRow(size, i));
            }
            _places.AddRange(InitDownBar(size));
            return _places;
        }

        private List<IPlace> InitRow(int x, int y)
        {
            var newList = new List<IPlace>();
            string character;
            newList.Add(new Place("@", y, new Symbol(y.ToString("D2"))));
            for (int i = 0; i < x; i++)
            {
                character = $"{(char)('A' + i)}";
                newList.Add(new Place(character, y));
            }
            character = $"{(char)('A' + x)}";
            newList.Add(new Place(character, y, new Symbol(Environment.NewLine)));

            return newList;
        }
        private List<IPlace> InitUpBar(int x)
        {
            var newList = new List<IPlace>();
            string character;
            newList.Add(new Place("@", 0, new Symbol("  ")));
            for (int i = 0; i < x; i++)
            {
                character = $"{(char)('A' + i)}";
                newList.Add(new Place(character, 0, new Symbol(character)));
            }
            character = $"{(char)('A' + x)}";
            newList.Add(new Place(character, 0, new Symbol(Environment.NewLine)));

            return newList;
        }

        private List<IPlace> InitDownBar(int x)
        {
            var newList = new List<IPlace>();
            string character;
            newList.Add(new Place("@", x + 1, new Symbol("")));
            for (int i = 0; i < x; i++)
            {
                character = $"{(char)('A' + i)}";
                newList.Add(new Place(character, x + 1, new Symbol("")));
            }
            character = $"{(char)('A' + x)}";
            newList.Add(new Place(character, x + 1, new Symbol(Environment.NewLine)));

            return newList;
        }
    }
}
