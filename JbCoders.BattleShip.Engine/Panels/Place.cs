﻿using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Interfaces.Entities;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Panels
{
    public class Place : IPlace
    {
        public Place(string x, int y)
        {
            X = x;
            Y = y;
        }

        public Place(string x, int y, IEntity entity)
        {
            X = x;
            Y = y;
            _entity = entity;
        }
        private IEntity _entity;
        public string X { get; }

        public int Y { get; }

        public bool IsSetable
        {
            get
            {
                if (_entity == null)
                {
                    return true;
                }
                if (_entity.IsSetable)
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsFireable => _entity.IsFireable;

        public void Draw()
        {
            Console.Write(" ");
            if (_entity == null)
            {
                Console.Write("*");
                return;
            }
            _entity.Draw();
        }

        public bool SetEntity(IEntity entity)
        {
            if (IsSetable)
            {
                _entity = entity;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Fire()
        {
            if (IsFireable)
            {
                _entity.Fire();
                return true;
            }
            return false;
        }

        public void MakeUnsetable()
        {
            _entity?.MakeUnsetable();
        }
    }
}
