﻿using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Interfaces.Panels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.BattleShip.Engine.Panels
{
    public class Panel : IPanel
    {
        private readonly List<IPlace> _places;
        private readonly int _size;
        private readonly bool _cover;
        public Panel(IPlacesFactory placesFactory, int size, bool cover = false)
        {
            if (size > 26)
            {
                throw new ArgumentException("Cant build Panel bigger than 26", nameof(size));
            }
            _size = size;
            _cover = cover;
            _places = placesFactory.Init(_size);
        }
        public void Draw()
        {
            foreach (var place in _places)
            {
                place.Draw();
            }
        }

        public IReadOnlyList<IPlace> Places => _places.AsReadOnly();
        public void FillWater()
        {
            foreach (var place in _places)
            {
                if (place.IsSetable)
                {
                    place.SetEntity(new Water());
                }
            }
        }

        private IPlace FindPlace(string x, int y)
        {
            return _places.FirstOrDefault(place => place.X == x && place.Y == y);
        }

        public bool Fire(string x, int y)
        {
            var placeToFire = FindPlace(x, y);
            if (placeToFire == null || !placeToFire.IsFireable) return false;
            return placeToFire.Fire();
        }

        private bool SetShipSegment(IPlace place, bool cover)
        {
            if (place == null || !place.IsSetable) return false;
            place.SetEntity(new Ship(cover));
            return true;
        }

        private void SetUnsetableSegment(string x, int y)
        {
            char character = x[0];
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    FindPlace($"{ (char)(character - i)}", y - j).MakeUnsetable();
                }
            }
        }

        private bool CanSetShip(string x, int y, int size, bool direction)
        {
            char character = x[0];
            for (int i = 0; i < size; i++)
            {
                if (direction)
                {
                    if (!FindPlace($"{ (char)(character + i)}", y).IsSetable) return false;
                }
                else
                {
                    if (!FindPlace(x, y + i).IsSetable) return false;
                }

            }
            return true;
        }

        private void SetUnsetable(string x, int y, int size, bool direction)
        {
            char character = x[0];
            for (int i = 0; i < size; i++)
            {
                if (direction)
                {
                    SetUnsetableSegment($"{ (char)(character + i)}", y);
                }
                else
                {
                    SetUnsetableSegment(x, y + i);
                }

            }
        }

        public bool SetShip(string x, int y, int size, bool direction)
        {
            if (!CanSetShip(x, y, size, direction)) return false;
            char character = x[0];
            for (int i = 0; i < size; i++)
            {
                if (direction)
                {
                    if (!SetShipSegment(FindPlace($"{ (char)(character + i)}", y), _cover)) return false;
                }
                else
                {
                    if (!SetShipSegment(FindPlace(x, y + i), _cover)) return false;
                }
            }
            SetUnsetable(x, y, size, direction);
            return true;
        }
    }
}
