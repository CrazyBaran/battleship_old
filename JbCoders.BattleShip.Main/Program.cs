﻿using JbCoders.BattleShip.Engine;
using System;

namespace JbCoders.BattleShip.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            var battleShip = new BattleShipGame();
            battleShip.Start();
        }
    }
}
